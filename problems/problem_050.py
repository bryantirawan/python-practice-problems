# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(list): #list = [5, 6, 7, 8, 9, 10, 11]
    length = len(list) #length = 7

    if length % 2 == 0: 
        firsthalf = length/2 - 1 #3rd index or i = 2 
    else: 
        firsthalf = (length + 1)/2 - 1         # 4th index or i = 3 

    
    result = [] 
    list_firsthalf = [] 
    list_secondhalf = []

    for i in range(length): # range(length) = 0, 1, 2, 3 
        if i <= firsthalf: 
            list_firsthalf.append(list[i])
        elif i > firsthalf: 
            list_secondhalf.append(list[i])
    

    #join list_firsthalf and list_secondhalf     
    result.append(list_firsthalf) 
    result.append(list_secondhalf)

    return result 



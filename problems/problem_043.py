# Complete the find_indexes function which accepts two
# parameters, a list and a search term. It returns a new
# list that contains the indexes of the search term in
# the search list.
#
# Remember that indexes in Python are zero-based. That
# means the first element in the list is index 0.
#
# Examples:
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  4
#     result:       [3]
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  6
#     result:       []
#   * search_list:  [1, 2, 1, 2, 1]
#     search_term:  1
#     result:       [0, 2, 4]
#
# Look up the enumerate function to help you with this problem.

def find_indexes(search_list, search_term):
    # #search_list = [1, 2, 3, 4, 5]
    # for number in search_list: 
    #     #number = 1 
    #     if number == search_term: 
    #         enumerate(search_list, )
    result = []
    for i in range(len(search_list)):
        # i = 0 and will go through 0,1,2,3,4 while len(search_list) = 5
        if search_list[i] == search_term: 
            result.append(i) 
    return result 


    pass


def find_indexes(search_list, search_term):
    results = []                                        # solution
    for index, value in enumerate(search_list):         # solution
        if value == search_term:                        # solution
            results.append(index)                       # solution
    return results                                      # solution
    # pass                                              # problem


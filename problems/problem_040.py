# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.
#
# There is pseudocode to guide you.

def add_csv_lines(csv_lines):
    # result_list = new empty list 
    result_list = []

    for item in csv_lines: 
        item_split = item.split(",")
        sum = 0
        for digit in item_split: 
            digit_postint = int(digit)
            sum = sum + digit_postint 
        result_list.append(sum)
    
    return result_list 

    # for each item in the csv_lines
        # pieces = split the item on the comma
        # line_sum = 0
        # for each piece in pieces
            # value = convert the piece into an int
            # add the value to sum
        # append sum to the result_list
    # return result_list
    pass

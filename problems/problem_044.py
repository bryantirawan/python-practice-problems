# Complete the translate function which accepts two
# parameters, a list of keys and a dictionary. It returns a
# new list that contains the values of the corresponding
# keys in the dictionary. If the key does not exist, then
# the list should contain a None for that key.
#
# Examples:
#   * keys:       ["name", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     ["Noor", 29]
#   * keys:       ["eye color", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [None, 29]
#   * keys:       ["age", "age", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [29, 29, 29]
#
# Remember that a dictionary has the ".get" method on it.

def translate(key_list, dictionary):
    result = []
    for target in key_list:
        #target = "name" and key_list = ["name", "age"]
        for key in dictionary: 
            if target == key: 
                #key = "name" in dictionary 
                result.append(dictionary[key]) 
                #need way to end this target and move on to next target 
                break
                #break accomplishes this but now target = "age" but key is still "name"
            else: 
                result.append(None)

    # result = [] 
    # for key in key_list: 
    #     result.append(dictionary.get(key))
    # return result  




  

        

    pass

# Write a function that meets these requirements.
#
# Name:       group_cities_by_state
# Parameters: a list of cities in the format "«name», «st»"
#             where «name» is the name of the city, followed
#             by a comma and a space, then the two-letter
#             abbreviation of the state
# Returns:    a dictionary whose keys are the two letter
#             abbreviations of the states in the list and
#             whose values are a list of the cities appearing
#             in that list for that state
#
# In the items in the input, there will only be one comma.
#
# Examples:
#     * input:   ["San Antonio, TX"]
#       returns: {"TX": ["San Antonio"]}
#     * input:   ["Springfield, MA", "Boston, MA"]
#       returns: {"MA": ["Springfield", "Boston"]}
#     * input:   ["Cleveland, OH", "Columbus, OH", "Chicago, IL"]
#       returns: {"OH": ["Cleveland", "Columbus"], "IL": ["Chicago"]}
#
# You may want to look up the ".strip()" method for the string.

def group_cities_by_state(list): #list = ["Springfield, MA", "Boston, MA"]
    dict = {}
    for item in list: # item = "Springfield, MA" 
        split = item.split(",") # split = ['Springfield', 'MA']
        state = split[1] # state = 'MA' 
        city = split[0] # city = 'Springfield' 

        if dict[state] == None: 
            dict[state] = []
        else: 
            dict[state].append(city)
    
    return dict 
             

def group_cities_by_state(cities):          # solution
    output = {}                             # solution
    for city in cities:                     # solution
        name, state = city.split(",")       # solution
        state = state.strip()               # solution
        if state not in output:             # solution
            output[state] = []              # solution
        output[state].append(name)          # solution
    return output                           # solution


        
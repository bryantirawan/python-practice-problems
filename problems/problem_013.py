# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three
# items.
#
# There is pseudocode to guide you.


def can_make_pasta(ingredients):
    # If "flour" is in ingredients and "eggs" is
    # in ingredients and "oil" is in ingredients
        # return True
    # Otherwise
        # return False
    
    if ingredients[0] == "flour" and ingredients[1] == "eggs" and ingredients[2] == "oil":
        return True 
    elif ingredients[0] == "flour" and ingredients[1] == "oil" and ingredients[2] == "eggs":
        return True 
    elif ingredients[0] == "eggs" and ingredients[1] == "flour" and ingredients[2] == "oil":
        return True 
    elif ingredients[0] == "eggs" and ingredients[1] == "oil" and ingredients[2] == "flour":
        return True 
    elif ingredients[0] == "oil" and ingredients[1] == "flour" and ingredients[2] == "eggs":
        return True
    elif ingredients[0] == "oil" and ingredients[1] == "eggs" and ingredients[2] == "flour":
        return True 
    else: 
        return False 
    pass

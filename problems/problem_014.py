# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three
# items.
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.

def can_make_pasta(ingredients):
    pass_list = []
    for ingredient in ingredients: 
        if ingredient == "flour" or ingredient == "oil" or ingredient == "eggs": 
            pass_list.append(ingredient)
    
    pass_list.sort()
    if pass_list == ["eggs", "flour", "oil"]:
        return True
    else:
        return False  
    pass

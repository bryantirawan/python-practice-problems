# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password): 
    one_lowercase = False 
    one_uppercase = False 
    one_digit = False 
    one_special = False 
    six_ormoore = False 
    twelve_orless = False 

    if len(password) >= 6 and len(password) <= 12: 
        six_ormoore = True 
        twelve_orless = True 
    
    for char in password: 
        if char.islower(): 
            one_lowercase = True 
        elif char.isupper(): 
            one_uppercase = True 
        elif char.isdigit(): 
            one_digit = True 
        elif char == "$" or char == "!" or char == "@": 
            one_special = True 
    
    if one_lowercase and one_uppercase and one_digit and one_special and six_ormoore and twelve_orless: 
        return True 
    else: 
        return False 
        

    pass
